<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/15/2020
  Time: 12:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <script src="farsitype.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<script>
    function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8 ||key==47 ||key==46 || key==58 || key==59 ||key==60 ||key==63 ||key==62 ||key==123||key==125||key==96||key==93||key==91);
    };
</script>
<script>
    $("#txtn").on('change keyup paste keydown', function(e) {
        if(just_persian(e.key) === false)
            e.preventDefault();
    });


    function just_persian(str) {
        var p = /^[\u0600-\u06FF\s]+$/;
        if (!p.test(str)) {
            return false
        }
        return true;
    }
</script>
<script>
    function allLetter(inputtxt)
    {
        var letters = /[أ-يa-zA-Z]/;
        if(inputtxt.value.match(letters))
        {
            alert('Your name have accepted ');
            return true;
        }
        else
        {
            alert('Please input persian characters only');
            return false;
        }
    }
</script>
<form action="/savecompanycustomer.do">
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
    <input type="text" name="companyName" placeholder="COMPANY NAME"  onkeydown="return alphaOnly(event)" >
    <%--<input type="text" name="companyName" placeholder="COMPANY NAME" Lang="fa-IR">--%>
    <input type="number" name="economicId" placeholder="ECONOMIC ID">
    <input type="date" name="dateOfRegistering" placeholder="DATE OF REGISTERING">
    <input type="submit" value="regisring company">
</form>
</br>
</body>
</html>
