package controller;

import model.entity.PersonCustomer;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/1/2020.
 */
@WebServlet("/save.do")
public class Save extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PersonCustomer person = new PersonCustomer(req.getParameter("name"), req.getParameter("lastName")
                , req.getParameter("identityNumber"), req.getParameter("nameOfFather"), req.getParameter("dateOfBirth"));
        try {
            PersonCustomerService.save(person);
            resp.sendRedirect("/showingcustomernumber.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
