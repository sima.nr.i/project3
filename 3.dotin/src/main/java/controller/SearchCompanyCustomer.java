package controller;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;
import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 8/16/2020.
 */ @WebServlet("/searchcompanycustomer.do")
public class SearchCompanyCustomer extends HttpServlet{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CompanyCustomer companyCustomer;
            if (req.getParameter("id")==""||req.getParameter("id")==null)
            {
                companyCustomer=new CompanyCustomer(-1,req.getParameter("companyname"),
                        req.getParameter("economicid"));
            }
            else
            companyCustomer=new CompanyCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("companyname"),
                    req.getParameter("economicid"));
            CompanyCustomerService companyCustomerService = new CompanyCustomerService();
            List<CompanyCustomer> companyCustomers=new ArrayList<CompanyCustomer>();
            companyCustomers=companyCustomerService.search(companyCustomer);
            req.setAttribute("list2", companyCustomerService.search(companyCustomer));
            req.getRequestDispatcher("/managing_companycustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
