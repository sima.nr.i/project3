package controller;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;
import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/savecompanycustomer.do")
public class SaveCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompanyCustomer companyCustomer = new CompanyCustomer(req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            CompanyCustomerService companyCustomerService = new CompanyCustomerService();
            companyCustomerService.save(companyCustomer);
            resp.sendRedirect("/showingcustomernumberforcompany.do");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
