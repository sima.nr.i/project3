package controller;

import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/deletecompanycustomer.do")
public class DeleteCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            CompanyCustomerService companyCustomerService = new CompanyCustomerService();
            companyCustomerService.delete(Integer.parseInt(req.getParameter("id")));
            resp.sendRedirect("/findallcompanycustomer.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
