package controller;

import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/11/2020.
 */
@WebServlet("/showingcustomernumberforcompany.do")
public class CustomerNumberForCompany extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CompanyCustomerService companyCustomerService = new CompanyCustomerService();
            req.setAttribute("customernumberforcompany", companyCustomerService.showCustomerNumber());
            req.getRequestDispatcher("/showingcustomernumberforcompany.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
