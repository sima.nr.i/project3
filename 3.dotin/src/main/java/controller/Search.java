package controller;

import model.entity.PersonCustomer;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/16/2020.
 */
@WebServlet("/search.do")
public class Search extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PersonCustomer personCustomer;
            if (req.getParameter("id")=="")
            {
                personCustomer=new PersonCustomer(-1,req.getParameter("name"),
                        req.getParameter("lastname"),req.getParameter("identitynumber"));

            }
            else
            personCustomer=new PersonCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("name"),
                    req.getParameter("lastname"),req.getParameter("identitynumber"));
            PersonCustomerService personCustomerService = new PersonCustomerService();
            req.setAttribute("list", personCustomerService.search(personCustomer));
            req.getRequestDispatcher("/managing_personcustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
