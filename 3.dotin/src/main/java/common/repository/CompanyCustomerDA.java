package model.repository;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class CompanyCustomerDA {
    private PreparedStatement preparedStatement;
    private Connection connection;
    private int countCustomers = 0;
    private List<Integer> customernumbers = new ArrayList<Integer>();

    public CompanyCustomerDA() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:mysql://localhost/dotin3?" + "user=sima&password=myjava123");

    }

    public void insert(CompanyCustomer companyCustomer) throws Exception {
        preparedStatement = connection.prepareStatement("insert into companycustomer" +
                " (customernumber,companyname,economicid,dateofregistering)" +
                " values (?,?,?,?)");
        countCustomers++;
        for (int i = 100; i < 1000; i++) {
            customernumbers.add(new Integer(i));
        }
        Collections.shuffle(customernumbers);
        preparedStatement.setString(1, String.valueOf(customernumbers.get(countCustomers)));
        preparedStatement.setString(2, companyCustomer.getCompanyName());
        preparedStatement.setString(3, companyCustomer.getEconomicId());
        preparedStatement.setString(4, companyCustomer.getDateOfRegistering());
        preparedStatement.executeUpdate();
    }

    public String selectCustomerNumber() throws Exception {
        preparedStatement = connection.prepareStatement("SELECT customernumber FROM dotin3.companycustomer ORDER BY id DESC LIMIT 0, 1\n");
        ResultSet resultSet = preparedStatement.executeQuery();
        String customerNumber = null;
        while (resultSet.next()) {
            customerNumber = resultSet.getString("customernumber");
        }
        return customerNumber;
    }

    public List<CompanyCustomer> select() throws Exception {
        preparedStatement = connection.prepareStatement("select * from companycustomer ");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers = new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
            companyCustomers.add(companyCustomer);
        }
        return companyCustomers;
    }

    public List<CompanyCustomer> search(CompanyCustomer companyCustomer)throws Exception{
        preparedStatement=connection.prepareStatement("select * FROM dotin3.companycustomer " +
                "WHERE (1=? or id=?) AND (1=? or companyname=?)AND (1=? or economicid=?) ");
        if (companyCustomer.getId()==-1)
        {
            preparedStatement.setInt(1,1);
        }
        else preparedStatement.setInt(1,0);
        preparedStatement.setInt(2,companyCustomer.getId());
        if (companyCustomer.getCompanyName()==null ||companyCustomer.getCompanyName()=="")
        {
            preparedStatement.setInt(3,1);
        }
        else preparedStatement.setInt(3,0);
        preparedStatement.setString(4,companyCustomer.getCompanyName());
        if (companyCustomer.getEconomicId()==null ||companyCustomer.getEconomicId()=="")
        {
            preparedStatement.setInt(5,1);
        }
        else preparedStatement.setInt(5,0);
        preparedStatement.setString(6,companyCustomer.getEconomicId());
        ResultSet resultSet=preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers=new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer customer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));

            companyCustomers.add(customer);
        }
        return companyCustomers;
    }
    public CompanyCustomer searchbyid(int id) throws Exception {
        preparedStatement=connection.prepareStatement("select * from companycustomer WHERE id =?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        CompanyCustomer companyCustomer=null;
        while (resultSet.next()) {
             companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
        }
        return companyCustomer;
    }
    public void delete(int id) throws Exception {
        preparedStatement = connection.prepareStatement("delete from companycustomer where id=?");
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    public void update(CompanyCustomer companyCustomer) throws Exception {
        CompanyCustomer companyCustomer1 =searchbyid(companyCustomer.getId());
        preparedStatement = connection.prepareStatement
                ("update dotin3.companycustomer set dotin3.companycustomer.companyname=?,dotin3.companycustomer.economicid=?," +
                        "dotin3.companycustomer.dateofregistering=? where dotin3.companycustomer.id=?");
        if(companyCustomer.getCompanyName()==null || companyCustomer.getCompanyName()=="")
            preparedStatement.setString(1,companyCustomer1.getCompanyName());
        else
            preparedStatement.setString(1, companyCustomer.getCompanyName());
        if (companyCustomer.getEconomicId()==null || companyCustomer.getEconomicId()=="")
            preparedStatement.setString(2,companyCustomer1.getEconomicId());
        else
            preparedStatement.setString(2, companyCustomer.getEconomicId());
        if (companyCustomer.getDateOfRegistering()==null || companyCustomer.getDateOfRegistering()=="")
            preparedStatement.setString(3,companyCustomer1.getDateOfRegistering());
        else
            preparedStatement.setString(3, companyCustomer.getDateOfRegistering());

        preparedStatement.setInt(4,companyCustomer.getId());
        preparedStatement.executeUpdate();
    }

    public void close() throws Exception {
        preparedStatement.close();
        connection.close();
    }
}
