package model.service;

import model.entity.PersonCustomer;
import model.repository.PersonCustomerDA;

import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class PersonCustomerService {
    public static void save(PersonCustomer person) throws Exception {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        personCustomerDA.insert(person);
        personCustomerDA.close();
    }
    public  String showCustomerNumber()throws Exception
    {
        PersonCustomerDA personCustomerDA=new PersonCustomerDA();
        String customerNumber=personCustomerDA.selectCustomerNumber();
        personCustomerDA.close();
        return customerNumber;
    }
    public static List<PersonCustomer> findAll()throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.select();
        personCustomerDA.close();
        return personList;
    }

    public  List<PersonCustomer> search(PersonCustomer personCustomer)throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.search(personCustomer);
        personCustomerDA.close();
        return personList;
    }
    public void update(PersonCustomer person) throws Exception {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        personCustomerDA.update(person);
        personCustomerDA.close();
    }
    public static void delete(int id) throws Exception {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        personCustomerDA.delete(id);
        personCustomerDA.close();
    }
}
