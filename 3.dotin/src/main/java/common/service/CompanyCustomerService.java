package model.service;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;
import model.repository.CompanyCustomerDA;
import model.repository.PersonCustomerDA;

import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class CompanyCustomerService {
    public static void save(CompanyCustomer companyCustomer) throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        companyCustomerDA.insert(companyCustomer);
        companyCustomerDA.close();
    }

    public String showCustomerNumber() throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        String customerNumber = companyCustomerDA.selectCustomerNumber();
        companyCustomerDA.close();
        return customerNumber;
    }

    public static List<CompanyCustomer> findAll() throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        List<CompanyCustomer> companyCustomers = companyCustomerDA.select();
        companyCustomerDA.close();
        return companyCustomers;
    }


    public  List<CompanyCustomer> search(CompanyCustomer companyCustomer) throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        List<CompanyCustomer> companyCustomers = companyCustomerDA.search(companyCustomer);
        companyCustomerDA.close();
        return companyCustomers;
    }
    public void update(CompanyCustomer companyCustomer) throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        companyCustomerDA.update(companyCustomer);
        companyCustomerDA.close();
    }

    public static void delete(int id) throws Exception {
        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
        companyCustomerDA.delete(id);
        companyCustomerDA.close();
    }
}
