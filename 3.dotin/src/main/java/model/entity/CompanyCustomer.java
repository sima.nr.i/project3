package model.entity;

import java.io.Serializable;

/**
 * Created by Hi on 8/1/2020.
 */
public class CompanyCustomer implements Serializable {
    private int id;
    private String customerNumber, companyName, economicId, dateOfRegistering;

    public CompanyCustomer(int id, String customernumber, String companyname, String economicid, String dateofregistering) {
        this.id = id;
        this.customerNumber = customernumber;
        this.companyName = companyname;
        this.economicId = economicid;
        this.dateOfRegistering = dateofregistering;
    }

    public CompanyCustomer(String customerNumber, String companyName, String economicId, String dateOfRegistering) {
        this.customerNumber = customerNumber;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    //================================================================================================================================
    public CompanyCustomer(String companyName, String economicId, String dateOfRegistering) {
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public CompanyCustomer(int id, String companyName, String economicId, String dateOfRegistering) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public CompanyCustomer(int id, String companyName, String economicId) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEconomicId() {
        return economicId;
    }

    public void setEconomicId(String economicId) {
        this.economicId = economicId;
    }

    public String getDateOfRegistering() {
        return dateOfRegistering;
    }

    public void setDateOfRegistering(String dateOfRegistering) {
        this.dateOfRegistering = dateOfRegistering;
    }
}
