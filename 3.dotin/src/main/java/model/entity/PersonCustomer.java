package model.entity;

import java.io.Serializable;

/**
 * Created by Hi on 8/1/2020.
 */
public class PersonCustomer implements Serializable {
    private Integer id;
    private String customerNumber, name, lastName, identityNumber, nameOfFather, dateOfBirth;

    public PersonCustomer(int id, String customerNumber, String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.id = id;
        this.customerNumber = customerNumber;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    //==================================================================================================================================
    public PersonCustomer(String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public PersonCustomer(int id, String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public PersonCustomer(int id, String name, String lastName, String identityNumber) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
    }

    //=====================================================================================================================================
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfFather() {
        return nameOfFather;
    }

    public void setNameOfFather(String nameOfFather) {
        this.nameOfFather = nameOfFather;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
